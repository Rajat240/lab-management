import {Injectable} from '@angular/core'
import {Router , CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router'
import {AuthenticationService} from './authentication.service'
import { Observable } from 'rxjs'


@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private auth: AuthenticationService , private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
     
    const expectedRole = route.data.expectedRole
    const mainUrl = route.data.mainUrl
    if (this.auth.isLoggedin() && expectedRole == this.auth.getRole()) {
           return true;
      }else{
        this.router.navigateByUrl('/');
        return false;
    }
  }

  } 
 

 
