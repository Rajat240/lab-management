import { Component, OnInit } from '@angular/core';
import {AuthenticationService , UserDetails} from '../authentication.service'

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.css']
})
export class DoctorComponent implements OnInit {
 constructor(private auth: AuthenticationService){}

  ngOnInit() {
  }

}
