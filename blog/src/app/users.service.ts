import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {Observable , of} from 'rxjs'
import {Router } from '@angular/router'

export interface UserDetails  {
  id: number
  name: string
  email: string
  password: string
  role_id: number
  lab_id: number

}

export interface UserPayload{
  id: number
  name: string
  email: string
  password: string
  role_id: number
  lab_id: number
}

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private token: string
  private role: string


  constructor(private http: HttpClient, private router: Router) { }

  private saveRole(role: string): void {
    localStorage.setItem('userrole', role)
    this.role = role
  }

  private getToken(): string{
    if(!this.token){
       this.token = localStorage.getItem('usertoken')
      }
      return this.token
    }

    public getRole(): string{
      if(!this.role){
         this.role = localStorage.getItem('userrole')
        }
        return this.role
      }

 public getUsers(user: UserDetails): Observable<any> {
      return this.http.post('/api/users/', user, {
      headers: {Authorization : `Bearer ${this.getToken()}`}
      })
    }

    public getDoctors(user: UserDetails): Observable<any> {
      return this.http.post('/api/doctors/', user, {
      headers: {Authorization : `Bearer ${this.getToken()}`}
      })
    }

  public add_user(user: UserPayload ): Observable<any>  {
    return this.http.post('/api/add_users' ,  user, {
      headers: {'Content-Type': 'application/json'}
    })
     }

  public delete_user(user: UserPayload ): Observable<any>  {
      console.log(user)
     return this.http.post('/api/delete_user/'+user,user , {
       headers: {Authorization : `Bearer ${this.getToken()}`}
     })
   }

   public delete_dr(user: UserPayload ): Observable<any>  {
    console.log(user)
   return this.http.post('/api/delete_dr/'+user,user , {
     headers: {Authorization : `Bearer ${this.getToken()}`}
   })
 }

   public get_user(user: UserPayload ): Observable<any>  {
    //console.log(user)
   return this.http.post('/api/get_user/'+user, user, {
     headers: {Authorization : `Bearer ${this.getToken()}`}
   })
 }

   public edit_user(user: UserPayload ): Observable<any> {
     //console.log(user)
       return this.http.post('/api/edit_user/'+user , user, {
     headers: {Authorization : `Bearer ${this.getToken()}`}
   })
  }
}
