import {Component} from '@angular/core'
import {Router} from '@angular/router'
import {AuthenticationService , TokenPayload} from '../authentication.service'


@Component({
  templateUrl: './register.component.html'
})


export class RegisterComponent {

  credentials: TokenPayload = {
      id: 0,
      name: "",
      email: "",
      password: "",
	  labname:""
  }

constructor(private auth: AuthenticationService, private router: Router){}
ngOnInit() {
  if (this.auth.isLoggedin()) {
        const current_role = this.auth.getRole();
       if(current_role == '1' ){
        this.router.navigateByUrl('/admin')
     }
     if(current_role == '2' ){
             this.router.navigateByUrl('/doctor')
      }
  }
}
register(){
        this.auth.register(this.credentials).subscribe(
          ()=> {
            this.router.navigateByUrl('/login')
          },
          err => {
            console.error(err)
          }
        )
  }
}
