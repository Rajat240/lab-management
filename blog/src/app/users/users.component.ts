import { Component, ViewChild, OnInit } from '@angular/core';
declare var $: any;
import {Router} from '@angular/router'
import { NgForm } from '@angular/forms'
import { UsersService, UserDetails, UserPayload } from '../users.service'

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  user_data: UserDetails
  users: UserDetails

  user_payload: UserPayload = {
    id: 0,
    name: "",
    email: "",
    password: "",
    role_id: 2,
    lab_id: 1

  }

  constructor(private user: UsersService, private router: Router) {}

  @ViewChild(NgForm) myForm: NgForm;

  ngOnInit() {

    $('#mdl-save').click(function() {
      $('#success').show();
      setTimeout(() => {
        $('#myModal').modal('hide');
        $('#success').hide();
      }, 1000);
    });
    $('#edit-save').click(function() {
      $('#updt-success').show();
      setTimeout(() => {
        $('#editModal').modal('hide');
        $('#updt-success').hide();
      }, 1000);
    });
    
  	this.getUsersdata();
  }

  getUsersdata(){
      this.user.getUsers().subscribe((res : any) => {
        this.users = res.data;
        //console.log(res)
      },
      err => {
        console.error(err)
      }
      )
  }

    add_user(){
      this.user.add_user(this.user_payload).subscribe(
        ()=> {
          const current_role = this.user.getRole();
          if(current_role == '1'){
          this.router.navigateByUrl('/superadmin/admin')
          this.getUsersdata()
        }
        if(current_role == '2' ){
          this.router.navigateByUrl('/admin/doctor')
          this.getUsersdata()
        }
         if(current_role == '3' ){
           this.router.navigateByUrl('/doctor/users')
           this.getUsersdata()
         }
         this.myForm.resetForm();
        },
        err => {
          console.error(err)
        }
      )
    }

    delete_user(id){
      this.user.delete_user(id).subscribe(
        (res : any) => {
          this.users = res.data
        },
        err => {
          console.error(err)
        }
      )
    } 

    get_user(id){
      this.user.get_user(id).subscribe(
        (res : any) => {

          this.user_payload = res.users
          //console.log(this.user_payload)
          //this.getUsersdata()
        },
        err => {
          console.error(err)
        }
      )
}

    update_user(){
      this.user.edit_user(this.user_payload).subscribe(
        (res : any) => {
          this.users = res.users
          //console.log(res)
          this.getUsersdata()
        },
        err => {
          console.error(err)
        }
      )
    } 

}
