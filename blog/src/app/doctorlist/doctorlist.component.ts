import { Component, ViewChild, OnInit } from '@angular/core';
declare var $: any;
import {Router} from '@angular/router'
import { NgForm } from '@angular/forms'
import { UsersService, UserDetails, UserPayload } from '../users.service'

@Component({
  selector: 'app-doctorlist',
  templateUrl: './doctorlist.component.html',
  styleUrls: ['./doctorlist.component.css']
})
export class DoctorlistComponent implements OnInit {

  
  user_data: UserDetails
  users: UserDetails

  user_payload: UserPayload = {
    id: 0,
    name: "",
    email: "",
    password: "",
    role_id: 3,
    lab_id: 1

  }

  constructor(private user: UsersService, private router: Router) {}

  @ViewChild(NgForm) myForm: NgForm;

  ngOnInit() {

    $('#drmdl-save').click(function() {
      $('#drsuccess').show();
      setTimeout(() => {
        $('#drmyModal').modal('hide');
        $('#drsuccess').hide();
      }, 1000);
    });
    $('#dredit-save').click(function() {
      $('#drupdt-success').show();
      setTimeout(() => {
        $('#dreditModal').modal('hide');
        $('#drupdt-success').hide();
      }, 1000);
    });
    
  	this.getDrdata();
  }

  getDrdata(){
      this.user.getDoctors().subscribe((res : any) => {
        this.users = res.data;
        //console.log(res)
      },
            err => {
              console.error(err)
            }
          )
    }

    add_user(){
      this.user.add_user(this.user_payload).subscribe(
        ()=> {
          const current_role = this.user.getRole();
          if(current_role == '1'){
          this.router.navigateByUrl('/superadmin/doctor')
          this.getDrdata()
        }
        if(current_role == '2' ){
          this.router.navigateByUrl('/admin/doctor')
          this.getDrdata()
        }
         if(current_role == '3' ){
           this.router.navigateByUrl('/doctor/users')
           this.getDrdata()
         }
         this.myForm.resetForm();
        },
        err => {
          console.error(err)
        }
      )
    }

    delete_dr(id){
      this.user.delete_dr(id).subscribe(
        (res : any) => {
          this.users = res.data
        },
        err => {
          console.error(err)
        }
      )
    } 

    get_user(id){
      this.user.get_user(id).subscribe(
        (res : any) => {

          this.user_payload = res.users
          //console.log(this.user_payload)
          //this.getUsersdata()
        },
        err => {
          console.error(err)
        }
      )
}

    update_user(){
      this.user.edit_user(this.user_payload).subscribe(
        (res : any) => {
          this.users = res.users
          //console.log(res)
          this.getDrdata()
        },
        err => {
          console.error(err)
        }
      )
    } 


}
