import { Component } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';

@Component({

  templateUrl: './home.component.html'

})

export class HomeComponent {
  constructor(private auth: AuthenticationService, private router: Router){}
  ngOnInit() {
    if (this.auth.isLoggedin()) {
          const current_role = this.auth.getRole();
         if(current_role == '1' ){
          this.router.navigateByUrl('/admin')
       }
       if(current_role == '2' ){
               this.router.navigateByUrl('/doctor')
        }
    }
  }
}
