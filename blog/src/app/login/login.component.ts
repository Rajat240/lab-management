import {Component} from '@angular/core'
import {Router} from '@angular/router'
import {AuthenticationService , TokenPayload} from '../authentication.service'

@Component({

  templateUrl: './login.component.html'

})
export class LoginComponent {

  credentials: TokenPayload = {
      id: 0,
      name: "",
      email: "",
      password: "",
      labname:""
  }

constructor(private auth: AuthenticationService, private router: Router){}
ngOnInit() {
  if (this.auth.isLoggedin()) {
        const current_role = this.auth.getRole();
      if(current_role == '1' ){
        this.router.navigateByUrl('/superadmin')
      }
      if(current_role == '2' ){
             this.router.navigateByUrl('/admin')
      }
       if(current_role == '3' ){
         this.router.navigateByUrl('/doctor')
  }
  }
}
  login(){
        this.auth.login(this.credentials).subscribe(
       (data)=> {
          if(data.role ==1 ){
            this.router.navigateByUrl('/superadmin')
         }
         if(data.role == 2 ){
                 this.router.navigateByUrl('/admin')
          }
           if(data.role == 3 ){
             this.router.navigateByUrl('/doctor')
      }
        },
          err => {
            console.error(err)
          }
        )
  }
}
