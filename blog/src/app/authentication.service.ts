import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {Observable , of} from 'rxjs'
import {map} from 'rxjs/operators'
import {Router } from '@angular/router'


export interface UserDetails  {
  id: number
  name: string
  email: string
  password: string
  exp: number
  iat: number
  role_id:number
}

interface TokenResponse{
  token: string,
  role:string
}

export interface TokenPayload{
  id: number
  name: string
  email: string
  password: string
  labname:string
}

@Injectable()
export  class AuthenticationService{
  private token: string
   private role: string

constructor(private http: HttpClient, private router: Router){}
  private saveToken(token: string): void {
    localStorage.setItem('usertoken', token)
    this.token = token
  }
  private saveRole(role: string): void {
    localStorage.setItem('userrole', role)
    this.role = role
  }


private getToken(): string{
if(!this.token){
   this.token = localStorage.getItem('usertoken')
  }
  return this.token
}
public getRole(): string{
if(!this.role){
   this.role = localStorage.getItem('userrole')
  }
  return this.role
}

public getUserDetails(): UserDetails{
  const token = this.getToken()
  let payload
  if(token){
    payload = token.split('.')[1]
    payload = window.atob(payload)
    return JSON.parse(payload)
  }else{
    return null
  }
}

 

public isLoggedin(): boolean {
  const user = this.getUserDetails()
  const role = this.getRole()
 if(user && role){
   return user.exp > Date.now() / 1000
   }else{
   return false
   }
}

public register(user: TokenPayload ): Observable<any>  {
  return this.http.post( '/api/register' , user , {
    headers: {'Content-Type': 'application/json'}
  })
}


public login(user: TokenPayload ): Observable<any>  {

  const base =  this.http.post(
                  '/api/login' ,
                  {email: user.email , password: user.password} ,
                  {headers: {'Content-Type': 'application/json'}
            }
        )

          const request = base.pipe(
            map((data: TokenResponse ) => {
              console.log(data);
              if(data.token){
                this.saveToken(data.token)
              }
              if(data.role){
                  this.saveRole(data.role)
                } 
              return data
            })
          )
  return request
}



public profile(): Observable<any>  {
  return this.http.get('/api/profile' ,   {
    headers: {Authorization : `Bearer ${this.getToken()}`}
  })
}

public logout (): void{
  this.token = ''
   window.localStorage.clear();
   window.localStorage.empty();
  this.router.navigateByUrl('/')
}

}
