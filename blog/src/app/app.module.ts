import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http'
import {FormsModule} from '@angular/forms'
import {RouterModule , Routes} from '@angular/router'




import { AppComponent } from './app.component';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { AuthenticationService } from './authentication.service';
import { AuthGuardService } from './auth-guard.service';
import { PostComponent } from './post/post.component';
 
import { DashboardComponent } from './dashboard/dashboard.component';
import { DoctorComponent } from './doctor/doctor.component';
import { UsersComponent } from './users/users.component';
import { AdminComponent } from './admin/admin.component';
import { DoctorlistComponent } from './doctorlist/doctorlist.component';


const routes : Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
 
    
  {path: 'superadmin', component: DashboardComponent , 
  canActivate:[AuthGuardService] ,
   data: {expectedRole: 1, mainUrl : '/superadmin'},
      children : [ 
        {path: 'profile', component: ProfileComponent  },
        {path: 'post', component: PostComponent },
        { path: 'admin', component: UsersComponent },
        { path: 'doctor', component: DoctorlistComponent }
      ] ,
  },
  
  {path: 'admin', component: AdminComponent , canActivate:[AuthGuardService] ,
  data: {expectedRole: 2 , mainUrl : '/admin'},
      children : [ 
        {path: 'profile', component: ProfileComponent  },
        {path: 'post', component: PostComponent },
        { path: 'doctor', component: DoctorlistComponent }
      ] ,
	},

  {path: 'doctor', component: DoctorComponent , canActivate:[AuthGuardService] ,
  data: {expectedRole: 3 , mainUrl : '/doctor'},
      children : [ 
        {path: 'profile', component: ProfileComponent  },
        {path: 'post', component: PostComponent },
        { path: 'users', component: UsersComponent }
      ] ,
	},
  
]

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    PostComponent,
    
    DashboardComponent,
    
    DoctorComponent,
    
    UsersComponent,
    
    AdminComponent,
    
    DoctorlistComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],

  providers: [ AuthenticationService, AuthGuardService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
