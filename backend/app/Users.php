<?php

namespace App;
//use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use Session;


class Users
{
    function GetUsers(){
    	$id = Auth::id();
    	//print_r($id);
    	$users = DB::table('users')->get();
    	return $users;
    }
}
