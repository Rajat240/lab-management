<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Lab;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Contracts\JWTSubject;

use Tymon\JWTAuth\PayloadFactory;
use Tymon\JWTAuth\JWTManager as JWT;


class UserController extends Controller
{
  public function register(Request $request)
   {
           $validator = Validator::make($request->json()->all() , [
           'name' => 'required|string|max:255',
           'email' => 'required|string|email|max:255|unique:users',
           'password' => 'required|string|min:6'
          
       ]);
       if($validator->fails()){
               return response()->json($validator->errors()->toJson(), 400);
       }
	    $lab = Lab::create(['name'=>$request->json()->get('labname')]);
	
        $user = User::create([
           'name' => $request->json()->get('name'),
           'email' => $request->json()->get('email'),
           'password' => Hash::make($request->json()->get('password')),
		   'lab_id'=> $lab->id,
		   'role_id'=> 1
       ]);
       $token = JWTAuth::fromUser($user);
       return response()->json(compact('user','token'),201);
   }

   public function login(Request $request)
   {
       $credentials = $request->json()->all();
	 
       try {
           if (! $token = JWTAuth::attempt($credentials)) {
               return response()->json(['error' => 'invalid_credentials'], 400);
           }else{
			   $user =User::select('role_id')->where('email',$request->json()->get('email'))->first();
				$role = $user->role_id;
		   }
       } catch (JWTException $e) {
           return response()->json(['error' => 'could_not_create_token'], 500);
       }
       return response()->json( compact('token','role') );
   }

   public function getAuthenticatedUser()
   {
       try {
           if (! $user = JWTAuth::parseToken()->authenticate()) {
               return response()->json(['user_not_found'], 404);
           }
       } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
           return response()->json(['token_expired'], $e->getStatusCode());
       } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
           return response()->json(['token_invalid'], $e->getStatusCode());
       } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
           return response()->json(['token_absent'], $e->getStatusCode());
       }
       return response()->json(compact('user'));
   }
}
