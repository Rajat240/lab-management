<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;
use App\Lab;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Contracts\JWTSubject;

use Tymon\JWTAuth\PayloadFactory;
use Tymon\JWTAuth\JWTManager as JWT;
use Auth;

class UsersController extends Controller
{
    public function getusers(Request $request){
    	  try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
               return response()->json(['user_not_found'], 404);
            } else{
              //$id =  Auth::id();
              $user = Auth::user();
              $role = $user->role_id;
            if($role == 1){
              $data = DB::table('users')->where('role_id', 2)->get();
		          //$posts = DB::table('posts')->get();
              return response()->json(compact('data'));
            }
            if($role == 2){
              $data = DB::table('users')->where('role_id', 3)->get();
              //$posts = DB::table('posts')->get();
              return response()->json(compact('data'));
            }
            if($role == 3){

            }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
          return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
          return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
          return response()->json(['token_absent'], $e->getStatusCode());
        }
    }

    public function getdoctors(Request $request){
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
               return response()->json(['user_not_found'], 404);
            } else{
              //$id =  Auth::id();
              $user = Auth::user();
              $role = $user->role_id;
            if($role == 1){
              $data = DB::table('users')->where('role_id', 3)->get();
              //$posts = DB::table('posts')->get();
              return response()->json(compact('data'));
            }
            if($role == 2){
              $data = DB::table('users')->where('role_id', 3)->get();
              //$posts = DB::table('posts')->get();
              return response()->json(compact('data'));
            }
            if($role == 3){

            }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
          return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
          return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
          return response()->json(['token_absent'], $e->getStatusCode());
        }
    }


    public function add_user(Request $request)
    {
      // $validator = Validator::make($request->json()->all() , [
      //      'name' => 'required|string|max:255',
      //      'email' => 'required|string|email|max:255|unique:users',
      //      'password' => 'required|string|min:6',
           
          
      //  ]);
      //  if($validator->fails()){
      //          return response()->json($validator->errors()->toJson(), 400);
      //  }
      // $lab = Lab::create(['name'=>$request->json()->get('lab_id')]);
      // $role = Lab::create(['name'=>$request->json()->get('role_id')]);
  
      $user = User::create([
        'name' => $request->json()->get('name'),
        'email' => $request->json()->get('email'),
        'password' => Hash::make($request->json()->get('password')),
        'lab_id'=> $request->json()->get('lab_id'),
        'role_id'=> $request->json()->get('role_id')
        ]);
        $token = JWTAuth::fromUser($user);
        return response()->json(compact('user','token'),201);
    } 

    public function delete_user(Request $request)
      {
        try {
          if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
          } else{
            $delete = DB::table('users')->where('id', $request->id)->delete();

            $user = Auth::user();
            $role = $user->role_id;
            if($role == 1){
              $data = DB::table('users')->where('role_id', 2)->get();
              //$posts = DB::table('posts')->get();
              return response()->json(compact('data'));
            }
            if($role == 2){
              $data = DB::table('users')->where('role_id', 3)->get();
              //$posts = DB::table('posts')->get();
              return response()->json(compact('data'));
            }
            if($role == 3){

            }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
          return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
          return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
          return response()->json(['token_absent'], $e->getStatusCode());
        }
      }

      public function delete_dr(Request $request)
      {
        try {
          if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
          } else{
            $delete = DB::table('users')->where('id', $request->id)->delete();

            $user = Auth::user();
            $role = $user->role_id;
            if($role == 1){
              $data = DB::table('users')->where('role_id', 3)->get();
              //$posts = DB::table('posts')->get();
              return response()->json(compact('data'));
            }
            if($role == 2){
              $data = DB::table('users')->where('role_id', 3)->get();
              //$posts = DB::table('posts')->get();
              return response()->json(compact('data'));
            }
            if($role == 3){

            }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
          return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
          return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
          return response()->json(['token_absent'], $e->getStatusCode());
        }
      }

      public function get_user(Request $request)
   {
     try {
           if (! $user = JWTAuth::parseToken()->authenticate()) {
               return response()->json(['user_not_found'], 404);
           }else{
            $users = DB::table('users')->where('id', $request->id)->first();
    //  $posts = DB::table('posts')->get();
            return response()->json(compact('users'));
           }
       } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
           return response()->json(['token_expired'], $e->getStatusCode());
       } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
           return response()->json(['token_invalid'], $e->getStatusCode());
       } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
           return response()->json(['token_absent'], $e->getStatusCode());
       }
 }

    public function edit_user(Request $request)
      {
        // print_r($request);
        // exit;
        try {
          if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
          } else{
            $c_user = DB::table('users')->where('id', $request->id)->update(['name' => $request->name ]);
            $user = DB::table('users')->get();
            return response()->json(compact('user'));
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
          return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
          return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
          return response()->json(['token_absent'], $e->getStatusCode());
        }
      } 
}
