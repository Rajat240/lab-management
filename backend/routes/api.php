<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register', 'UserController@register');
Route::post('login', 'UserController@login');
Route::get('profile', 'UserController@getAuthenticatedUser');
Route::get('posts', 'PostController@index');
Route::post('add_post', 'PostController@add_post');
Route::post('delete_post/{id}', 'PostController@delete_post');
Route::post('get_post/{id}', 'PostController@get_post');
Route::post('edit_post/{id}', 'PostController@edit_post');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('users', 'UsersController@getusers');
Route::get('doctors', 'UsersController@getdoctors');
Route::post('add_users', 'UsersController@add_user');
Route::post('delete_user/{id}', 'UsersController@delete_user');
Route::post('delete_dr/{id}', 'UsersController@delete_dr');
Route::post('get_user/{id}', 'UsersController@get_user');
Route::post('edit_user/{id}', 'UsersController@edit_user');